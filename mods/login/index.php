<?php 

session_start();

if(isset($_GET["id"]) and $_GET["id"] == "out"):
    $_SESSION["user"]       = null;
    $_SESSION               = null;
    session_destroy();
endif;

if(isset($_SESSION["usuario"]) && $_SESSION["usuario"] != ""){
    header("Location: ../dashboard/index");
}

#--- llama a funciones
require_once("../../required/functions.php");

#--- leer variables globales
$Gd_json      = json_decode(file_get_contents("../../required/config.json"));
$Gl_appName   = $Gd_json->{"appName"};
$Gl_appUrl    = $Gd_json->{"appUrl"};

$Gd_user      = "";
$Gd_password  = "";
$Gd_error     = "";

if( isset($_POST["correo"]) && isset($_POST["password"]) ):
    $Gd_user      = $_POST["correo"];
    $Gd_password  = $_POST["password"];

    require_once("../../required/gl-conexion_bd.php");
    require_once("login.php");
    
    $log  = new Login();
    $ret  = $log->login($Gd_user, $Gd_password);

    if(!$ret->fallo):

        #--- asigna objeto completo a la sesión
        $_SESSION["usuario"]["id"]              = $ret->id;
        $_SESSION["usuario"]["nombre"]          = $ret->nombre;
        $_SESSION["usuario"]["paterno"]         = $ret->paterno;
        $_SESSION["usuario"]["materno"]         = $ret->materno;
        $_SESSION["usuario"]["correo"]          = $ret->correo;
        $_SESSION["usuario"]["estado"]          = $ret->estado;
        $_SESSION["usuario"]["idPerfil"]        = $ret->idPerfil;
        $_SESSION["usuario"]["perfil"]          = $ret->nomPerfil;
        
        switch ($ret->estado) :
            case "R": #--- cuando cambió la contraseña
                header("Location: ../recuperar-contrasenna/form");
                break;
            case "A": #--- cuando ingresa correctamente al sistema
                header("Location: ../dashboard/index");
                break;
            default:
                $Gd_error = "Usuario y/o contraseña incorrectas";
                break;
        endswitch;
    else:
        $Gd_error = "Usuario y/o contraseña incorrectas";
    endif;
endif;
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>easyMetrics | Log in</title>

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <link rel="stylesheet" href="<?=$Gl_appUrl ?>/plugins/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?=$Gl_appUrl ?>/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?=$Gl_appUrl ?>/plugins/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="<?=$Gl_appUrl ?>/assets/css/AdminLTE.min.css">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="#"><b>easy</b>Metrics </a>
            </div>
            
            <div class="login-box-body">
                <?php if($Gd_error != ""): ?>
                <div class="alert alert-danger fadeIn">
                    <?= $Gd_error ?>
                </div>
                <?php endif; ?>

                <p class="login-box-msg">Ingresa tus datos para empezar...</p>

                <form action="<?=$Gl_appUrl ?>/login/index" method="post">
                    <div class="form-group has-feedback">
                        <input type="input" class="form-control" placeholder="Correo" name="correo" id="correo" autocomplete="off" autofocus>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" placeholder="Contraseña" name="password" id="password">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
                        </div>
                    </div>
                </form>

                <div class="social-auth-links text-center">
                    <a href="<?= $Gd_json->{"appUrl"} ?>/recuperar-contrasenna/index">Olvidé mi contraseña</a><br>
                </div>
            </div>
        </div>

        <script src="<?=$Gl_appUrl ?>/plugins/jquery/dist/jquery.min.js"></script>
        <script src="<?=$Gl_appUrl ?>/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>
