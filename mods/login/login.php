<?php
class Login{

    public $correo;
    public $nombre;
    public $paterno;
    public $materno;
    public $id;
    public $fallo;
    public $idPerfil;
    public $nomPerfil;
    
    function __construct(){}

    function login($email, $password){
        $conn        = new connbd();
        $strconn     = $conn->connect();

        #--- limpia las variables
        $email       = clear($email, $strconn);
        $password    = clear($password, $strconn);

        $sql        = "SELECT email, estado, t1.id, materno, t1.nombre, paterno, id_perfil, t2.nombre as nomPerfil ";
        $sql       .= "FROM usuarios t1 ";
        $sql       .= "INNER JOIN perfiles t2 ON t1.id_perfil = t2.id "; 
        $sql       .= "where email = '". $email . "' and password = '" . $password . "' and estado = 'A' ";
  
        $resultado  = $strconn->query($sql);
        
        $login = new Login();

        if($resultado):
            if($resultado->num_rows > 0):
                $row                = $resultado->fetch_assoc();
                $login->correo      = $row["email"];
                $login->estado      = $row["estado"];
                $login->id          = $row["id"];
                $login->materno     = $row["materno"];
                $login->nombre      = $row["nombre"];
                $login->paterno     = $row["paterno"];
                $login->idPerfil    = $row["id_perfil"];
                $login->nomPerfil   = $row["nomPerfil"];
                $login->fallo       = false;
            else:
                $login->fallo       = true;
            endif;
        else:
            $login->fallo          = true;
        endif;


        $strconn->close();

        return $login;
    }
}

?>
