<?php 
    require_once('../../required/header.php');
?>

<section class="content-header">
  <h1>
    Creación Informe [nombre] - <small>Etapa Perímetros</small> 
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/informes/index">Informes</a></li>
    <li class="active">
        Crear Informe
    </li>
  </ol>
</section>


<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Perímetros:</h3> 
        </div>

        <form action="" role="form" class="form-horizontal">
            <div class="box-body">
                <!--Fila Cabeza -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Cabeza</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1" id="">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila cuello -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Cuello</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila brazo relajado -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Brazo Relajado</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila brazo flex -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Brazo Flex, Tensión</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila antebrazo max -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Antebrazo Máx.</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila torax -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Tórax</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila cintura -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Cintura mín.</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila cadera máx -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Cadera max.</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>                                                                                                


                <!--Fila muslo medio -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Muslo medio</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>      

                <!--Fila pantorrilla -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Pantorrilla</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>      




                <!--Fila tobillo -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Tobillo</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>      


                <!--Fila muslo -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Muslo</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>      




            </div>

            <div class="box-footer">
                <a href="<?=$Gl_appUrl?>/informes/form-diametros/1" class="btn btn-primary pull-right">Continuar...</a>
            </div>
        </form>

    </div>
</section>


<?php require_once("../../required/footer.php");?>
<?php require_once("../../required/scripts.php"); ?>
