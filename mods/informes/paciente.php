<?php 
    class Pliegues{

    }

    class Paciente { 
        public $id;
        public $rut;
        public $nombres;
        public $apellidos;
        public $actividad;
        public $sexo;
        public $fechaNacimiento;
        public $pacienteInformacion;

        #--- guarda o modifica (si existe) un paciente
        function guardarPaciente(){
            if($this->existePaciente()){
                $this->modificarPaciente();
            }else{
                $this->registrarPaciente();
            }
        }

        #--- registra un paciente en la bd
        function registrarPaciente(){
            #--- datos de conexión
            $conn        = new connbd();
            $strconn     = $conn->connect();

            #--- limpia la variable
            $this->rut                  = clear($this->rut, $strconn);
            $this->nombres              = clear($this->nombres, $strconn);
            $this->apellidos            = clear($this->apellidos, $strconn);
            $this->actividad            = clear($this->actividad, $strconn);
            $this->sexo                 = clear($this->sexo, $strconn);
            $this->fechaNacimiento      = clear($this->fechaNacimiento, $strconn);
            
            $sql    =  "INSERT INTO pacientes
                                    (rut, nombre, fecha_nacimiento, apellidos, sexo, actividad) 
                            VALUES(
                                    '$this->rut',
                                    '$this->nombres',
                                    '$this->fechaNacimiento',
                                    '$this->apellidos',
                                    '$this->sexo',
                                    '$this->actividad'
                                )";

            $strconn->query($sql) or die("Error insertando paciente: ". mysqli_error($strconn));                 
            $this->id   = $strconn->insert_id;

            $strconn->close();
        }

        #--- modifica un paciente
        function modificarPaciente(){
            #--- datos de conexión
            $conn        = new connbd();
            $strconn     = $conn->connect();

            #--- limpia la variable
            $this->rut                  = clear($this->rut, $strconn);
            $this->nombres              = clear($this->nombres, $strconn);
            $this->apellidos            = clear($this->apellidos, $strconn);
            $this->actividad            = clear($this->actividad, $strconn);
            $this->sexo                 = clear($this->sexo, $strconn);
            $this->fechaNacimiento      = clear($this->fechaNacimiento, $strconn);
            
            $sql    =  "UPDATE pacientes
                            SET 
                                nombre              = '$this->nombres',
                                fecha_nacimiento    = '$this->fechaNacimiento', 
                                apellidos           = '$this->apellidos', 
                                sexo                = '$this->sexo',
                                actividad           = '$this->actividad'
                            WHERE rut = '$this->rut' ";

            $strconn->query($sql) or die("Error modificando paciente: ". mysqli_error($strconn));  
            
            $this->id   = $this->getIdPaciente();

            $strconn->close();
        }

        #--- verifica la existencia de un paciente
        function existePaciente(){
            #--- datos de conexión
            $conn        = new connbd();
            $strconn     = $conn->connect();

            #--- limpia la variable
            $this->rut  = clear($this->rut, $strconn);

            $sql        = "SELECT count(id) as existe FROM pacientes WHERE rut = '$this->rut'";
            $existe     = false;
            $resultado  = $strconn->query($sql) or die("Error exists: ". mysqli_error($strconn));

            if($resultado->num_rows > 0):
                $row = $resultado->fetch_assoc();

                if($row["existe"]>0)
                    $existe = true;

            endif;

            $strconn->close();

            return $existe;
        }

        #--- obtiene un paciente desde la bd
        function getPaciente($rut){
            #--- datos de conexión
            $conn        = new connbd();
            $strconn     = $conn->connect();

            #--- limpia la variable
            $this->rut  = clear($this->rut, $strconn);

            $sql        = "SELECT id, rut, nombre, fecha_nacimiento, paterno, materno, sexo, actividad FROM pacientes WHERE rut = '$this->rut'";
            $resultado  = $strconn->query($sql) or die("Error exists: ". mysqli_error($strconn));
            $paciente   = new Paciente();
            
            if($resultado->num_rows > 0):
                $row = $resultado->fetch_assoc();
                
                #--- asigna valores a las propiedades del obj
                $paciente->id                   = $row["id"];
                $paciente->nombres              = $row["nombres"];
                $paciente->fechaNacimiento      = $row["fecha_nacimiento"];
                $paciente->paterno              = $row["paterno"];
                $paciente->materno              = $row["materno"];
                $paciente->sexo                 = $row["sexo"];
                $paciente->actividad            = $row["actividad"];
            endif;            

            $strconn->close();
            
            return $paciente;
        }


        public function getIdPaciente(){
            #--- datos de conexión
            $conn        = new connbd();
            $strconn     = $conn->connect();
            
            $sql         = "SELECT id FROM pacientes WHERE rut = '$this->rut'"; 

            $resultado  = $strconn->query($sql) or die("Error exists: ". mysqli_error($strconn));
            $id         = 0;

            if($resultado->num_rows > 0):
                $row = $resultado->fetch_assoc();
                $id  = $row["id"];    
            endif;

            $strconn->close();
            
            return $id;
        }
        
    }

    class PacienteInformacion{
        public $peso;
        public $envergadura;
        public $gastoKcal;
        public $talla;
        public $tallaSentado;
        public $altCajon;
        public $medTallaSentado;
        public $idPaciente;

        #--- registra un paciente en la bd
        function registrarInformacion(){
            #--- datos de conexión
            $conn        = new connbd();
            $strconn     = $conn->connect();

            #--- limpia la variable
            $this->peso                 = clear($this->peso, $strconn);
            $this->envergadura          = clear($this->envergadura, $strconn);
            $this->gastoKcal            = clear($this->gastoKcal, $strconn);
            $this->talla                = clear($this->talla, $strconn);
            $this->tallaSentado         = clear($this->tallaSentado, $strconn);
            $this->altCajon             = clear($this->altCajon, $strconn);
            $this->medTallaSentado      = clear($this->medTallaSentado, $strconn);

            
            $sql    =  "INSERT INTO paciente_informacion
                                    (peso, talla, talla_sentado, envergadura, gasto_kcal, id_paciente, altura_cajon, med_talla_sentado) 
                            VALUES(
                                    '$this->peso',
                                    '$this->talla',
                                    '$this->tallaSentado',
                                    '$this->envergadura',
                                    '$this->gastoKcal',
                                    '$this->idPaciente',
                                    '$this->altCajon',
                                    '$this->medTallaSentado'
                                )";

            $strconn->query($sql) or die("Error insertando info paciente: ". mysqli_error($strconn));                 
            $strconn->close();
        }        
    }


?>