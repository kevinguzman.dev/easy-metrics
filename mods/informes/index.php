
<?php require_once('../../required/header.php'); ?>

<section class="content-header">
    <h1>
      Informes
      <small>Todos</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= $Gl_appUrl ?>/dashboard/index"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Informes</li>
    </ol>
  </section>

  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <h3 class="box-title">Listado de todos los informes registrados</h3>
        <div class="box-tools">
          <a href="<?= $Gl_appUrl ?>/informes/form" class="btn btn-default">Agregar nuevo</a>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="alumnos" class="table table-striped responsive table-hover">
            <thead>
                <th>Fecha</th>
                <th>Paciente</th>
                <th>Acción</th>
            </thead>

            <tbody>
                <tr>
                    <td>14-12-2021 03:42:29</td>
                    <td>Kevin Guzmán Miranda</td>
                    <td>
                    <a href='<?=$Gl_appUrl?>/informes/form/1' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>
                    </td>
                </tr>

                <tr>
                    <td>14-12-2021 03:42:29</td>
                    <td>Kevin Guzmán Miranda</td>
                    <td>
                    <a href='<?=$Gl_appUrl?>/informes/form/1' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>
                    </td>
                </tr>

                <tr>
                    <td>14-12-2021 03:42:29</td>
                    <td>Kevin Guzmán Miranda</td>
                    <td>
                    <a href='<?=$Gl_appUrl?>/informes/form/1' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>
                    </td>
                </tr>

                <tr>
                    <td>14-12-2021 03:42:29</td>
                    <td>Kevin Guzmán Miranda</td>
                    <td>
                    <a href='<?=$Gl_appUrl?>/informes/form/1' class='btn btn-default' title='Editar'><i class='fa  fa-external-link'></i></a>
                    </td>
                </tr>                                                
            </tbody>

        </table>
      </div>
    </div>
  </section>
<?php require_once("../../required/footer.php"); ?>
<?php require_once("../../required/scripts.php"); ?>
