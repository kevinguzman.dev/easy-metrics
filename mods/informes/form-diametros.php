<?php 
    require_once('../../required/header.php');
?>

<section class="content-header">
  <h1>
    Creación Informe [nombre] - <small>Etapa Diámetros</small> 
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="<?=$Gl_appUrl?>/informes/index">Informes</a></li>
    <li class="active">
        Crear Informe
    </li>
  </ol>
</section>


<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Diámetros:</h3> 
        </div>

        <form action="" role="form" class="form-horizontal">
            <div class="box-body">
                <!--Fila Triceps -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Biacromial</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila Subescapular -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Tórax Transverso</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila bíceps -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Tórax Antero.</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila cresta ilìaca -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Biliocrestídeo</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila supraespinal -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Humeral</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila Abdominal -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Femoral</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila muslo frontal -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Muñeca (Biestiloidea)</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila pantorrilla -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Tobillo (Bimaleolar)</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 1">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 2">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" placeholder="Med 3">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>                                                                                                



            </div>

            <div class="box-footer">
                <a href="finalizar" class="btn btn-primary pull-right">Finalizar...</a>
            </div>
        </form>

    </div>
</section>


<?php require_once("../../required/footer.php");?>
<?php require_once("../../required/scripts.php"); ?>
