<?php 
require_once('../../required/header.php');
require_once('paciente.php');
require_once('informe.php');

$mostrarBoton           = false;
$linkSiguienteEtapa     = $Gl_appUrl."/informe/form-pliegues";

if(isset($_POST) && isset($_POST["rut"]) && $_POST["rut"] != ""){
    
    #--- registra al paciente
    $paciente                       = new Paciente();
    $paciente->rut                  = $_POST["rut"];
    $paciente->nombres              = $_POST["nombres"];
    $paciente->apellidos            = $_POST["apellidos"];
    $paciente->actividad            = $_POST["actividad"];
    $paciente->sexo                 = $_POST["sexo"];
    $paciente->fechaNacimiento      = dbd($_POST["fechaNacimiento"]);

    $paciente->guardarPaciente();

    #--- registra los datos del peso y altura del paciente
    $infoPaciente                   = new PacienteInformacion();
    $infoPaciente->peso             = $_POST["peso"];
    $infoPaciente->envergadura      = $_POST["envergadura"];
    $infoPaciente->gastoKcal        = $_POST["kcal"];
    $infoPaciente->talla            = $_POST["talla"];
    $infoPaciente->tallaSentado     = $_POST["tallaSentado"];
    $infoPaciente->altCajon         = $_POST["altCajon"];
    $infoPaciente->medTallaSentado  = $_POST["medTallaSentado"];
    $infoPaciente->idPaciente       = $paciente->id;

    $infoPaciente->registrarInformacion();
    
    #--- crea la cabecera del informe
    $informe                        = new Informe();
    $informe->idPaciente            = $paciente->id;
    $informe->idUsuario             = $_SESSION["usuario"]["id"];

    $informe->guardarInforme();
    $mostrarBoton                   = true;
    $linkSiguienteEtapa            .= "/".$informe->id; 
}
?>

<section class="content-header">
    <h1>
        Ingreso de variables <small>Datos del paciente</small> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=$Gl_appUrl?>/informes/index">Informes</a></li>
        <li class="active">
            Ingreso de variables
        </li>
    </ol>
</section>


<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Datos del Paciente:</h3> 
        </div>

        <form action="<?= $Gl_appUrl ?>/informes/form/" class="box-horizontal" method="POST">
            <div class="box-body">
                <!--Fila nombres -->
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="rut">Rut/Pasaporte</label>
                            <input type="text" class="form-control" placeholder="Rut o pasaporte del paciente" name="rut" id="rut" autocomplete="off">  
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="nomPaciente">Nombres</label>
                            <input type="text" class="form-control" placeholder="Nombres del paciente" name="nombres" id="nombres" autocomplete="off">
                        </div>
                    </div>
                    
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="apePaciente">Apellidos</label>
                            <input type="text" class="form-control" placeholder="Apellidos del paciente" name="apellidos" id="apellidos" autocomplete="off">
                        </div>
                    </div>
                </div>

                <!--fila sexo y fecha nacimiento -->
                <div class="row">

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="actPaciente">Actividad</label>
                            <input type="text" class="form-control" placeholder="Actividad del paciente" name="actividad" id="actividad" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="sexoPaciente">Sexo</label>
                            <div class="row"></div>
                            <label class="radio-inline">
                                <input type="radio" name="sexo" id="sexo" value="1" checked=""> Masculino
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="sexo" id="sexo" value="2" > Femenino
                            </label>
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="fechaNacimiento">Fecha de nacimiento</label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="fechaNacimiento" name="fechaNacimiento"  autocomplete="off">
                            </div>
                        </div>
                    </div>
                </div>

                <hr />
                <div class="row">
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="pesoPaciente">Peso</label>
                            <input type="text" class="form-control" placeholder="Peso (kg) del paciente" name="peso" id="peso" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="envergadura">Envergadura</label>
                            <input type="text" class="form-control" placeholder="Peso (kg) del paciente" name="envergadura" id="envergadura" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="kcal">Gasto en KCal</label>
                            <input type="text" class="form-control" placeholder="Peso (kg) del paciente" name="kcal" id="kcal" autocomplete="off">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="talla">Talla (cm)</label>
                            <input type="text" class="form-control" placeholder="Talla en centímetros del paciente" name="talla" id="talla" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="tallaSentado">Talla Sentado (cm)</label>
                            <input type="text" class="form-control" placeholder="Talla del paciente sentado" name="tallaSentado" id="tallaSentado" autocomplete="off"> 
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="altCajon">Altura Cajón</label>
                            <input type="text" class="form-control" placeholder="Altura del cajón donde se sentó el paciente" name="altCajon" id="altCajon" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-xs-3">
                        <div class="form-group">
                            <label for="medTallaSentado">Med. Talla-Sentado</label>
                            <input type="text" class="form-control" placeholder="Altura del paciente sentado" name="medTallaSentado" id="medTallaSentado" autocomplete="off"> 
                        </div>
                    </div>

                </div>
            </div>

            <div class="box-footer">
                <?php if($mostrarBoton): ?>
                <a href="<?=$linkSiguienteEtapa?>" class="btn btn-success pull-right">Siguiente ></a>
                <?php endif; ?>

                <button class="btn btn-primary pull-right" style="margin-right:3px;" type="submit">
                    Registrar datos
                </button>
            </div>
        </form>

    </div>
</section>


<?php require_once("../../required/footer.php");?>

<script>
$(function(){
    $('#fechaNacimiento').datepicker({
      autoclose: true,
      orientation: "bottom",
      language: 'es',
      endDate: '+0d',
    })

});
</script>
<?php require_once("../../required/scripts.php"); ?>
