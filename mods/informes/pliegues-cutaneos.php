<?php 
    class PlieguesCutaneos{
        public $triceps             = "";
        public $subescapular        = "";
        public $biceps              = "";
        public $crestaIliaca        = "";
        public $supraespinal        = "";
        public $abdominal           = "";
        public $musloFrontal        = "";
        public $pantorrilla         = "";
        public $etapaMedicion       = "";
        public $idInforme           = "";

        public function guardarPliegues(){
            #--- datos de conexión
            $conn        = new connbd();
            $strconn     = $conn->connect();

            $sql         = "INSERT INTO paciente_pliegues (triceps, subescapular, biceps, cresta_iliaca, supraespinal, abdominal, muslo_frontal, pantorrilla, medida, id_informe)
                                                    VALUES(
                                                        $this->triceps,
                                                        $this->subescapular,
                                                        $this->biceps,
                                                        $this->crestaIliaca,
                                                        $this->supraespinal,
                                                        $this->abdominal,
                                                        $this->musloFrontal,
                                                        $this->pantorrilla,
                                                        $this->etapaMedicion,
                                                        $this->idInforme
                                                    )"; 


            $strconn->query($sql) or die("Error insertando pliegues: ". mysqli_error($strconn). " <br> ".$sql);                 

            $strconn->close();
        }

        public function getPliegueCutaneo($idInforme, $idMedida){
            #--- datos de conexión
            $conn           = new connbd();
            $strconn        = $conn->connect();

            $sql            = "SELECT triceps, subescapular, biceps, cresta_iliaca, supraespinal, abdominal, muslo_frontal, pantorrilla, medida 
                                FROM paciente_pliegues 
                                WHERE id_informe = '$idInforme' AND medida = '$idMedida'";
            
            $resultado      = $strconn->query($sql) or die("Error trayendo pliegues: ". mysqli_error($strconn). " <br> ".$sql);                 
            
            $pliegue        = new PlieguesCutaneos();

            if($resultado):
                if($resultado->num_rows > 0):
                    $row                        = $resultado->fetch_assoc();
                    $pliegue->triceps           = $row["triceps"];
                    $pliegue->subescapular      = $row["subescapular"];
                    $pliegue->biceps            = $row["biceps"];
                    $pliegue->crestaIliaca      = $row["cresta_iliaca"];
                    $pliegue->supraespinal      = $row["supraespinal"];
                    $pliegue->abdominal         = $row["abdominal"];
                    $pliegue->musloFrontal      = $row["muslo_frontal"];
                    $pliegue->pantorrilla       = $row["pantorrilla"];
                endif;
            endif;

            $strconn->close();

            return $pliegue;
        }

        public static function getUltimoPliegue($idInforme){
            #--- datos de conexión
            $conn           = new connbd();
            $strconn        = $conn->connect();

            $sql            = "SELECT medida 
                                FROM paciente_pliegues 
                                WHERE id_informe = '$idInforme'";
            
            $resultado      = $strconn->query($sql) or die("Error trayendo pliegues: ". mysqli_error($strconn). " <br> ".$sql);        
            
            $medida         = 0;
            if($resultado):
                if($resultado->num_rows > 0):
                    $row                        = $resultado->fetch_assoc();
                    $medida                     = $row["medida"];
                endif;
            endif;            
            
            $strconn->close();

            return $medida;
        }

       #--- compara dos objetos, si no son iguales los agrega a un arreglo para posteriormente mostrarlos en pantalla 
        public static function compararObjetos($pliegue1, $pliegue2){
            $diferencias        = [];
    
            if($pliegue1->triceps <> $pliegue2->triceps){
                $dif            = new stdClass();
                $dif->id        = 1;
                $dif->nombre    = "Tríceps";

                $diferencias[] = $dif;
            }
    
            if($pliegue1->subescapular <> $pliegue2->subescapular){
                $dif            = new stdClass();
                $dif->id        = 2;
                $dif->nombre    = "Subescapular";

                $diferencias[] = $dif;
            }
    
            if($pliegue1->biceps <> $pliegue2->biceps){
                $dif            = new stdClass();
                $dif->id        = 3;
                $dif->nombre    = "Biceps";

                $diferencias[] = $dif;
            }
    
            if($pliegue1->crestaIliaca <> $pliegue2->crestaIliaca){
                $dif            = new stdClass();
                $dif->id        = 4;
                $dif->nombre    = "Cresta Ilíaca";

                $diferencias[] = $dif;
            }
    
            if($pliegue1->supraespinal <> $pliegue2->supraespinal){
                $dif            = new stdClass();
                $dif->id        = 5;
                $dif->nombre    = "Supra Espinal";
                
                $diferencias[] = $dif;
            }
    
            if($pliegue1->abdominal <> $pliegue2->abdominal){
                $dif            = new stdClass();
                $dif->id        = 6;
                $dif->nombre    = "Abdominal";
                
                $diferencias[] = $dif;
            }
    
            if($pliegue1->musloFrontal <> $pliegue2->musloFrontal){
                $dif            = new stdClass();
                $dif->id        = 7;
                $dif->nombre    = "Muslo Frontal";
                
                $diferencias[] = $dif;
            }
    
            if($pliegue1->pantorrilla <> $pliegue2->pantorrilla){
                $dif            = new stdClass();
                $dif->id        = 8;
                $dif->nombre    = "Pantorrilla";
                
                $diferencias[] = $dif;
            }
            
            return $diferencias;
        }
    
    
    }


?>