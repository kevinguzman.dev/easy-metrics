<?php 
require_once('../../required/header.php');
require_once('pliegues-cutaneos.php');

#--- variable que manejará las etapas de medición
$idInforme          = $_GET["id"];
$exito              = false;
$mensaje            = "";
$etapaMedicion      = 0;

$disabledEtapa1     = "";
$disabledEtapa2     = "disabled=''";
$disabledEtapa3     = "disabled=''";

$diferenciasObj     = [];
$diferenciasId      = [];
$diferenciasStr     = "";

#--- variables de medida pliegues cutáneos
$plieguesCutaneos1  = new PlieguesCutaneos();
$plieguesCutaneos2  = new PlieguesCutaneos();
$plieguesCutaneos3  = new PlieguesCutaneos();

#--- muestra los datos de los pliegues anteriormente registrados
if(isset($_GET["p3"]) && $_GET["p3"] >= 0){
    switch ($_GET["p3"]) {
        case 1:
            $plieguesCutaneos1  = $plieguesCutaneos1->getPliegueCutaneo($idInforme, 0);
            $plieguesCutaneos2  = $plieguesCutaneos1->getPliegueCutaneo($idInforme, 1);
            break;
        case 2:
            $plieguesCutaneos1  = $plieguesCutaneos1->getPliegueCutaneo($idInforme, 0);
            $plieguesCutaneos2  = $plieguesCutaneos1->getPliegueCutaneo($idInforme, 1);
            $plieguesCutaneos3  = $plieguesCutaneos1->getPliegueCutaneo($idInforme, 2);
            break;
        default:
            break;
    }
}

#---- registra primera etapa
if(isset($_POST) && isset($_POST["triceps1"]) && $_POST["triceps1"] != "" && isset($_GET["p3"]) && $_GET["p3"] == 0){
    $plieguesCutaneos1->triceps             = $_POST["triceps1"];
    $plieguesCutaneos1->subescapular        = $_POST["subescapular1"];
    $plieguesCutaneos1->biceps              = $_POST["biceps1"];
    $plieguesCutaneos1->supraespinal        = $_POST["supraespinal1"];
    $plieguesCutaneos1->abdominal           = $_POST["abdominal1"];
    $plieguesCutaneos1->musloFrontal        = $_POST["musloFrontal1"];
    $plieguesCutaneos1->pantorrilla         = $_POST["pantorrilla1"];
    $plieguesCutaneos1->crestaIliaca        = $_POST["crestaIliaca1"];

    $plieguesCutaneos1->idInforme           = $_GET['id'];
    $plieguesCutaneos1->etapaMedicion       = $etapaMedicion;

    #---- registra en la bd la primera etapa
    $plieguesCutaneos1->guardarPliegues();


    #--- logica para bloquear los campos etapa 2 y 3
    $etapaMedicion      = 1;
    $disabledEtapa1     = "disabled=''";
    $disabledEtapa2     = "";

} 

#---- registra segunda etapa
if(isset($_POST) && isset($_POST["triceps2"]) && $_POST["triceps2"] != "" && isset($_GET["p3"]) && $_GET["p3"] == 1){
    $plieguesCutaneos2->triceps             = $_POST["triceps2"];
    $plieguesCutaneos2->subescapular        = $_POST["subescapular2"];
    $plieguesCutaneos2->biceps              = $_POST["biceps2"];
    $plieguesCutaneos2->supraespinal        = $_POST["supraespinal2"];
    $plieguesCutaneos2->abdominal           = $_POST["abdominal2"];
    $plieguesCutaneos2->musloFrontal        = $_POST["musloFrontal2"];
    $plieguesCutaneos2->pantorrilla         = $_POST["pantorrilla2"];
    $plieguesCutaneos2->crestaIliaca        = $_POST["crestaIliaca2"];

    $plieguesCutaneos2->idInforme           = $_GET['id'];
    $plieguesCutaneos2->etapaMedicion       = PlieguesCutaneos::getUltimoPliegue($idInforme)+1;

    #---- registra en la bd la primera etapa
    $plieguesCutaneos2->guardarPliegues();

    #--- logica para bloquear los campos etapa 2 y 3
    $etapaMedicion      = 2;
    $disabledEtapa1     = "disabled=''";
    $disabledEtapa2     = "disabled=''";
    

    #---- analiza los datos ingresados, si existen diferencias, debe ingresar a la etapa 3
    $diferenciasObj     = PlieguesCutaneos::compararObjetos($plieguesCutaneos1, $plieguesCutaneos2);

    if(count($diferenciasObj) > 0){
        for($i = 0; $i < count($diferenciasObj); $i++){
            if($i == 0){
                $diferenciasStr .= $diferenciasObj[$i]->nombre;
            }else{
                $diferenciasStr .= ", ".$diferenciasObj[$i]->nombre;
            }
            
            $diferenciasId[] = $diferenciasObj[$i]->id;
        }
    }else{
        $exito  = true;
    }
} 

#---- registra primera etapa
if(isset($_POST) && isset($_POST["triceps3"]) && $_POST["triceps3"] != "" && isset($_GET["p3"]) && $_GET["p3"] == 2){
    
    #---- analiza los datos ingresados, si existen diferencias, debe ingresar a la etapa 3
    $diferenciasObj     = PlieguesCutaneos::compararObjetos($plieguesCutaneos1, $plieguesCutaneos2);

    if(count($diferenciasObj) == 0){
        $disabledEtapa1     = "disabled=''";
        $disabledEtapa2     = "disabled=''";
        $exito              = true;
    }
} 


?>

<section class="content-header">
    <h1>
        Ingreso de variables <small>Pliegues Cutáneos</small> 
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i csadasdasdasdlass="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?=$Gl_appUrl?>/informes/index">Ingreso de variables</a></li>
        <li class="active">
            Pliegues Cutáneos
        </li>
    </ol>
</section>

<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Pliegues cutáneos:</h3> 
        </div>

        <form action="<?= $Gl_appUrl ?>/informes/form-pliegues/<?=$idInforme?>/<?= $etapaMedicion ?>" role="form" class="form-horizontal" method="post">
            <div class="box-body">

                <?php if($exito):?>
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Felicitaciones </h4>
                    <p> Se han registrado los pliegues cutáneos sin problemas, haga click en el botón azul para continuar.</p>
                </div>
                <?php endif;?>

                <?php if( count($diferenciasObj) > 0 ):?>
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-warning"></i> Cuidado </h4>
                        <p> Se encontraron diferencias en las siguientes medidas: <b><?= $diferenciasStr ?>. </b></p>
                    </div>
                <?php endif; ?>

                <!--Fila Triceps -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Tríceps</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1" name="triceps1" id="triceps1" value="<?= $plieguesCutaneos1->triceps ?>" <?= $disabledEtapa1 ?> >
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="triceps2" id="triceps2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->triceps ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="triceps3" id="triceps3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->triceps ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila Subescapular -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Subescapular</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1"  name="subescapular1" id="subescapular1" value="<?= $plieguesCutaneos1->subescapular ?>" <?= $disabledEtapa1 ?>>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="subescapular2" id="subescapular2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->subescapular ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                        $disabledEtapa3 = "disabled=''";
                                        if (in_array(2, $diferenciasId)) {
                                            $disabledEtapa3 = "";
                                        }
                                    ?>
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="subescapular3" id="subescapular3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->subescapular ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila bíceps -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Bíceps</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1" name="biceps1" id="biceps1" value="<?= $plieguesCutaneos1->biceps ?>" <?= $disabledEtapa1 ?>>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="biceps2" id="biceps2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->biceps ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                        $disabledEtapa3 = "disabled=''";
                                        if (in_array(3, $diferenciasId)) {
                                            $disabledEtapa3 = "";
                                        }
                                    ?>
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="biceps3" id="biceps3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->biceps ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila cresta ilìaca -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Cresta Ilíaca</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1" name="crestaIliaca1" id="crestaIliaca1" value="<?= $plieguesCutaneos1->crestaIliaca ?>" <?= $disabledEtapa1 ?>>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="crestaIliaca2" id="crestaIliaca2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->crestaIliaca ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                        $disabledEtapa3 = "disabled=''";
                                        if (in_array(4, $diferenciasId)) {
                                            $disabledEtapa3 = "";
                                        }
                                    ?>
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="crestaIliaca3" id="crestaIliaca3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->crestaIliaca ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>


                <!--Fila supraespinal -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Supraespinal</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1" name="supraespinal1" id="supraespinal1" value="<?= $plieguesCutaneos1->supraespinal ?>" <?= $disabledEtapa1 ?>>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="supraespinal2" id="supraespinal2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->supraespinal ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                        $disabledEtapa3 = "disabled=''";
                                        if (in_array(5, $diferenciasId)) {
                                            $disabledEtapa3 = "";
                                        }
                                    ?>
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="supraespinal3" id="supraespinal3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->supraespinal ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila Abdominal -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Abdominal</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1" name="abdominal1" id="abdominal1" value="<?= $plieguesCutaneos1->abdominal ?>" <?= $disabledEtapa1 ?>>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="abdominal2" id="abdominal2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->abdominal ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                        $disabledEtapa3 = "disabled=''";
                                        if (in_array(6, $diferenciasId)) {
                                            $disabledEtapa3 = "";
                                        }
                                    ?>
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="abdominal3" id="abdominal3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->abdominal ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila muslo frontal -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Muslo frontal</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1" name="musloFrontal1" id="musloFrontal1" value="<?= $plieguesCutaneos1->musloFrontal ?>" <?= $disabledEtapa1 ?>> 
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="musloFrontal2" id="musloFrontal2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->musloFrontal ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                        $disabledEtapa3 = "disabled=''";
                                        if (in_array(7, $diferenciasId)) {
                                            $disabledEtapa3 = "";
                                        }
                                    ?>
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="musloFrontal3" id="musloFrontal3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->musloFrontal ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>

                <!--Fila pantorrilla -->
                <div class="row">
                    <div class="col-md-2">
                        <label for="nombrePaciente" class="col-sm-2 control-label">Pantorrilla</label>
                    </div>

                    <div class="col-md-10">
                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 1" name="pantorrilla1" id="pantorrilla1" value="<?= $plieguesCutaneos1->pantorrilla ?>" <?= $disabledEtapa1 ?>>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="number" step="any" class="form-control" placeholder="Medida 2" name="pantorrilla2" id="pantorrilla2" <?= $disabledEtapa2 ?> value="<?= $plieguesCutaneos2->pantorrilla ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-4">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php
                                        $disabledEtapa3 = "disabled=''";
                                        if (in_array(8, $diferenciasId)) {
                                            $disabledEtapa3 = "";
                                        }
                                    ?>
                                    <input type="number" step="any" class="form-control" placeholder="Medida 3" name="pantorrilla3" id="pantorrilla3" <?= $disabledEtapa3 ?> value="<?= $plieguesCutaneos3->pantorrilla ?>">
                                </div>
                            </div>
                        </div>    
                    </div>                
                </div>                                                                                                



            </div>

            <div class="box-footer">
                <?php
                    $esconder = ""; 
                    if($exito): 
                        $esconder = "style='display:none;'";
                    endif;
                ?>
                
                <button class="btn btn-primary pull-right" type="submit" <?= $esconder ?>>Registrar </button>
                
                <?php if($exito):?>
                <a href="<?=$Gl_appUrl?>/informes/form-perimetros/<?=$idInforme?>" class="btn btn-success pull-right">Continuar</a>
                <?php endif;?>
            </div>
        </form>

    </div>
</section>


<?php require_once("../../required/footer.php");?>
<?php require_once("../../required/scripts.php"); ?>
