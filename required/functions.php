<?php 
#--- imprime arreglo en formato legible
function pre($arreglo, $detener){
    echo "<pre>";
    print_r($arreglo);
    echo "</pre>";

    if($detener):
        exit;
    endif;
}

#--- limpia la variable
function clear($variable, $mysqli){
    $ret = "";
    $ret = $mysqli->real_escape_string($variable);
    return $ret;
}

#--- retorna numero con formato miles
function miles($numero){
    return number_format($numero,  0, '', '.');
}

#--- retorna fecha en formato [dia]-[mes]-[año] [hora]:[minutos]:[segundos]
function dt($fecha){
    $fecha=date_create($fecha);
    return date_format($fecha,"d-m-Y H:i:s");
}
  
function d($fecha){
    $fecha=date_create($fecha);
    return date_format($fecha,"d-m-Y");
}

#--- retorna fecha en formato Y-m-d
function dbd($fecha){
    $fecha = date("Y-m-d", strtotime($fecha));
    return $fecha;
}

function validarAccesoModulo($id_perfil, $modulo){
    $conn        = new connbd();
    $strconn     = $conn->connect();
    $id_perfil   = clear($id_perfil, $strconn);
    $modulo      = clear($modulo, $strconn);
  
    $sql         = "SELECT count(id_perfil) as count FROM perfiles_menus WHERE id_perfil = ".$id_perfil." AND id_menu = ".$modulo;
    $res         = $strconn->query($sql) or die("error validar perfil: " . mysqli_error($strconn));
    $valida      = false;
  
    if($res->num_rows > 0): 
      $row = $res->fetch_assoc();
      if($row["count"] > 0):
        $valida = true;
      endif;
    endif;
  
    $strconn->close();
    return $valida;
  }


function EstructuraHtml()
{
    $html = '<html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">				
                    <title>Notificación de Correo Electrónico</title>
                </head>				
                <body>
                    <div style="font-family:Arial, Helvetica, sans-serif; background-color:#FAFAFA; width:90%; min-height:520px; padding-top: 20px; padding-left: 5%; padding-bottom: 20px; padding-right: 5%">
                        <div style="margin:0 auto; background-color:#ffffff; color:#505050; font-size:14px; max-width:570px; min-height:400px; font-family: Helvetica; line-height:150%; text-align:left; border:#E1E1E1 solid 1px">
                            <div style="background-color:#F8F8F8; max-width:570px; height:85px; border-bottom:#E1E1E1 solid 1px">
                                <!--<img src="css/images/logo-email.png" alt="" style="margin-top:20px; margin-left:20px "/> -->
                            </div>
                            <div style="padding:20px;">	
                                [*contenidoEmail*]
                            </div>
                            <div style="margin:20px 5px 10px 5px; text-align: center; font-size:11px;">
                                <!-- <a href="#">Visita nuestro sitio web</a> | <a href="#">¿Necesitas Ayuda?</a><br/> -->
                                Copyright © kGym
                            </div>						
                        </div>
                        <br/>
                    </div>
                </body>				
            </html>';
            
    return $html;
}
  
#--- envía un correo electrónico a través de PHPMailer
function EnviarCorreo($asunto, $correo, $mensaje){
    require 'mailer/Exception.php';
    require 'mailer/PHPMailer.php';
    require 'mailer/SMTP.php';

    date_default_timezone_set('America/Santiago');	
                            
    $mail               = new PHPMailer\PHPMailer\PHPMailer(true);

    $mail->SMTPDebug    = 0;                                                                      
    $mail->Host         = 'mail.ksolutions.cl';  
    $mail->SMTPAuth     = true;                               
    $mail->Username     = 'cicerocostha@ksolutions.cl';  
    $mail->Password     = 'lwgvsHJc0[Is';                     
    $mail->Port         = 465;  
    $mail->SMTPSecure   = 'ssl';     
    $mail->CharSet      = 'UTF-8';   
    $mail->Encoding     = 'base64';                          

    $mail->isSMTP(); 
    $mail->ClearAddresses();
    $mail->setFrom('cicerocostha@ksolutions.cl', 'Salud y Rendimiento');
    $mail->AddReplyTo( 'cicerocostha@ksolutions.cl', 'Cicero costha' );   
    $mail->isHTML(true);      
    $mail->addAddress($correo);  

    $mail->Subject = html_entity_decode(htmlentities($asunto));

    #--- cuerpo del correo
    $body             = EstructuraHtml();
    $body             = str_replace("[\]",'',$body);
    $body             = str_replace('[*contenidoEmail*]',nl2br($mensaje),$body);	

    $mail->Body    = $body;

    if(!$mail->send()){
        echo $mail->ErrorInfo;
    }
}

#--- genera string random
function generaString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
} 

#--- retorna el valor del sexo de una persona
function getSexo($sexo){
    switch ($sexo) {
        case 1:
            return "Masculino";
        case 2:
            return "Femenino";
        default:
            # code...
            break;
    }
}

#--- calcula la media (medida que más se repite) de dos propiedades de dos objetos
function calcularMedia($valor1, $valor2, $valor3=0){
    $media          = 0;
    $arrayDatos[]   = $valor1;
    $arrayDatos[]   = $valor2;
    
    if($valor3 <> 0)
        $arrayDatos[] = 0;

    #--- suma los datos
    $media          = array_sum($arrayDatos);
    
    if($valor3 <> 0)
        $media          = $media / 2;
    else
        $media          = $media / 3;

    return $media;
}

?>