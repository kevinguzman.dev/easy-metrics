<?php
  //trae la foto del usuario:
  $Gl_userFoto      = $Gl_appUrl."/assets/img/no-photo.png";
  $Gl_perfil        = 1;   
  $Gd_nomUsuario    = $_SESSION["usuario"]["nombre"] . " " .$_SESSION["usuario"]["paterno"] . " " .$_SESSION["usuario"]["materno"];
?>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="<?= $Gl_appUrl ?>/dashboard/index" class="logo">
                <span class="logo-mini"><?= $Gl_appNameMobile ?></span>
                <span class="logo-lg"><b>easy</b>Metrics</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!--notificaciones
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="fakse">
                                <i class="fa fa-bell-o">
                                    <span class="label label-danger">10</span>
                                </i>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="header">Tienes 10 notificaciones</li>
                                <ul class="menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-users text-aqua"></i>
                                            Se registro algo
                                        </a>
                                    </li>
                                </ul>
                            </ul>
                        </li>-->

                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?=$Gl_userFoto ?>" class="user-image" alt="User Image">
                                <span class="hidden-xs"><?= $Gd_nomUsuario  ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?=$Gl_userFoto ?>" class="img-circle" alt="User Image">
                                    <p>
                                    <?= $Gd_nomUsuario ?>
                                    </p>
                                </li>

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Perfil</a>
                                    </div>
                                    <div class="pull-right">
                                    <a href="<?=$Gl_appUrl ?>/login/index/out" class="btn btn-default btn-flat">Salir</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>


        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?=$Gl_userFoto ?>" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?= $Gd_nomUsuario ?></p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online / Perfil: <?= $_SESSION["usuario"]["perfil"] ?></a><br>
                    </div>
                </div>
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Menú / Opciones</li>
                    <li>
                        <a href="<?= $Gl_appUrl ?>/dashboard/index">
                            <i class="fa fa-dashboard"></i> <span>Escritorio</span>
                        </a>
                    </li>

                    <?php if(validarAccesoModulo($Gl_perfil, 1)): ?>
                    <li>
                        <a href="<?= $Gl_appUrl ?>/informes/index">
                            <i class="fa fa-edit"></i> <span>Informes</span>
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(ValidarAccesoModulo($Gl_perfil, 2)): ?>
                    <li class="treeview">
                        <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Usuarios</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= $Gl_appUrl ?>/usuarios/index"><i class="fa fa-circle-o"></i> Ver todos</a></li>
                            <li><a href="<?= $Gl_appUrl ?>/usuarios/form"><i class="fa fa-circle-o"></i> Registrar</a></li>
                        </ul>
                    </li>
                    <?php endif;?>
                </ul>
            </section>
        </aside>

        <div class="content-wrapper">
