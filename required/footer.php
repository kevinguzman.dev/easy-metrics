    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
        </div>
        <strong>Copyright &copy; 2021 <a href="https://www.ksolutions.cl">kSolutions</a>.</strong> Todos los derechos reservados
    </footer>
</aside>

<script src="<?=$Gl_appUrl ?>/plugins/SweetAlert/sweetalert2.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/SweetAlert/core.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/SweetAlert/Alerts.js"></script>

<script src="<?=$Gl_appUrl ?>/plugins/jquery/dist/jquery.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/select2/dist/js/select2.full.min.js"></script>

<script src="<?=$Gl_appUrl ?>/plugins/moment/min/moment.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
<script src="<?=$Gl_appUrl ?>/assets/js/adminlte.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/datatables.net-bs/js/dataTables.responsive.min.js"></script>
<script src="<?=$Gl_appUrl ?>/plugins/chart.js/Chart.js"></script>
                              
<script src="<?=$Gl_appUrl ?>/plugins/inputmask/dist/inputmask/jquery.inputmask.js"></script>

