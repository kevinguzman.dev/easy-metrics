<?php
    //verificar sesiones
    session_start();
    
    if( !isset($_SESSION["usuario"]) || !isset($_SESSION["usuario"]) || $_SESSION == null):
        header("Location: ../../login/index");
    endif;
    
    //llama a funciones
    require_once("functions.php");
    require_once("gl-conexion_bd.php");
    
    //leer variables globales
    $Gl_appName   = "";
    $Gl_appUrl    = "";

    $Gd_json            = json_decode(file_get_contents("../../required/config.json"));
    $Gl_appName         = $Gd_json->{"appName"};
    $Gl_appUrl          = $Gd_json->{"appUrl"};
    $Gl_appNameMobile   = $Gd_json->{"appNameMobile"};
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>easyMetrics </title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
        
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/Ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/datatables.net-bs/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/plugins/SweetAlert/sweetalert2.css">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/assets/css/AdminLTE.min.css?20210505">
        <link rel="stylesheet" href="<?= $Gl_appUrl ?>/assets/css/skins/_all-skins.min.css">

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

<?php require_once("sidebar.php"); ?>
