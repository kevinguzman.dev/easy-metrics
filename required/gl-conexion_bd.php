<?php
class connbd{
    function __construct(){}

    function connect(){
        $servername   = "localhost";
        $username     = "root";
        $password     = "root";

        // Create connection
        $conn = new mysqli($servername, $username, $password, "bd_easy_anthropometry", 3306);


        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error); exit;
        }

        $conn->set_charset("utf8");
        $conn->query("SET collation_connection = utf8_spanish_ci");

        return $conn;
    }
}

?>
